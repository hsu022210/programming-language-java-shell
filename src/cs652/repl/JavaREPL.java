package cs652.repl;

import java.io.*;
import javax.tools.*;
import java.util.*;
import java.lang.reflect.*;
import java.net.*;
import java.util.List;

public class JavaREPL {

	public static void main(String[] args) throws IOException {
		exec(new InputStreamReader(System.in));
	}


	public static void exec(Reader r) throws IOException {
		BufferedReader stdin = new BufferedReader(r);
		NestedReader reader = new NestedReader(stdin);

		int classNumber = 0;
		File temDir = createTempDirectory();
		String dir = temDir.getAbsolutePath() + "/";

		deleteFiles(temDir);
		temDir.mkdir();

		String content;
		String fileName;
		String className;
		Boolean compile_status;
		List fileArray = new ArrayList();

		URL tmpURL = temDir.toURI().toURL();
		ClassLoader loader = new URLClassLoader(new URL[]{tmpURL});

		while (true) {
			System.out.print("> ");

			content = reader.getNestedString();
			fileName = "Interp_" + classNumber + ".java";
			className = fileName.substring(0, fileName.lastIndexOf("."));


			writeFile(dir, fileName, classNumber, content, "declaration");

			fileArray.add(dir + fileName);

			compile_status = true;

			compile_status = compile(fileArray, compile_status);

			if (compile_status.equals(false)){
				writeFile(dir, fileName, classNumber, content, "statement");
				compile_status = compile(fileArray, compile_status);
			}

			if (compile_status){
				runExec(loader, className);
				classNumber++;
			}else {
				File tmp = new File(dir + fileName);
				deleteFiles(tmp);
				fileArray.remove(dir + fileName);
			}

			if (reader.getC() == -1){
				break;
			}
		}
	}


	//Below function's code modified from http://stackoverflow.com/questions/2885173/how-do-i-create-a-file-and-write-to-a-file-in-java
	public static void writeFile(String dir, String fileName, int classNumber, String content, String codeType){

		String extendSuper;

		if (classNumber != 0){

			int extendSuperNumberInt = classNumber-1;
			String extendSuperNumber = String.valueOf(extendSuperNumberInt);
			extendSuper = " extends Interp_" + extendSuperNumber;
		}else {
			extendSuper = "";
		}

		try{
			PrintWriter writer = new PrintWriter(dir +fileName, "UTF-8");
			String className = fileName.substring(0, fileName.lastIndexOf("."));
			String code = getCode(className, extendSuper, content, codeType);
			writer.println(code);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static String getCode(String className, String extendSuper, String content, String codeType){
		String result;

		if (codeType.equals("declaration")){
			content =
					"    public static " + content+ "\n" +
							"    public static void exec() {\n" +
							"    }\n";
		}else {
			content =
//					"    public static " + content+ "\n" +
					"    public static void exec() {\n" +
							"      " + content + "\n" +
							"    }\n";
		}

		result = "import java.io.*;\n" +
				"import java.util.*;\n" +
				"public class " + className + extendSuper +" {\n" +
				content + "}";
		return result;
	}


	//Below function's code modified from http://www.java2s.com/Code/Java/JDK-6/CompileaJavafilewithJavaCompiler.htm
	public static boolean compile(List fileArray, Boolean compile_status) throws IOException {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);
		Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromStrings(fileArray);
		JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, diagnostics, null, null, compilationUnits);

		boolean success = task.call();

		if (compile_status.equals(false)){
			List<Diagnostic> diagnosticsResult = (List)diagnostics.getDiagnostics();
			for(Diagnostic d: diagnosticsResult){
				System.out.println("line " + (d.getLineNumber()+2) + ": " + d.getMessage(Locale.ENGLISH));
			}
		}

		fileManager.close();
//		System.out.println("Success: " + success);
		return success;
	}


	public static void runExec(ClassLoader loader, String className) {

		try{
			Class cl = loader.loadClass(className);
			Method run = cl.getDeclaredMethod("exec");
			run.invoke(cl, null);

		}catch (ClassNotFoundException e){
			e.printStackTrace();
		}catch (NoSuchMethodException e){
			e.printStackTrace();
		}catch (IllegalAccessException e){
			e.printStackTrace();
		}catch (InvocationTargetException e){
			e.printStackTrace();
		}
	}

	//Below function's code modified from http://blog.csdn.net/love_ubuntu/article/details/6673722
	public static void deleteFiles(File file){
		if(file.exists() || file.isDirectory()) {
			if(file.isFile() || file.list().length ==0)
			{
				file.delete();
			}else{
				File[] files = file.listFiles();
				for (int i = 0; i < files.length; i++) {
					deleteFiles(files[i]);
					files[i].delete();
				}

				if(file.exists()) file.delete();
			}
		}

	}

	//Below function's code is from http://stackoverflow.com/questions/617414/how-to-create-a-temporary-directory-folder-in-java
	public static File createTempDirectory() throws IOException
	{
		final File temp;

		temp = File.createTempFile("temp", Long.toString(System.nanoTime()));

		if(!(temp.delete()))
		{
			throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
		}

		if(!(temp.mkdir()))
		{
			throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
		}

		return (temp);
	}
}
