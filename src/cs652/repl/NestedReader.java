package cs652.repl;

/**
 * Created by hsu022210 on 1/29/17.
 */

import java.io.*;
import java.util.*;

public class NestedReader {

    private static StringBuilder buf = new StringBuilder();    // fill this as you process, character by character
    private static BufferedReader input; // where are we reading from?
    private static int c; // current character of lookahead; reset upon each getNestedString() call
    private static Stack<Integer> stack = new Stack<>();
    private static int previous_c = -1;
    private static boolean inQuotation;
    private static boolean inDoubleQuotation;
    private static boolean inputInBlock;
    private static boolean inComment;
    private static boolean printTransform;

    public NestedReader(BufferedReader input) {
        this.input = input;
    }

    public int getC(){
        return c;
    }

    public String getNestedString() throws IOException {
        buf = new StringBuilder();
        c = input.read();
        inQuotation = false;
        inDoubleQuotation = false;
        inputInBlock = false;
        inComment = false;
        printTransform = false;

        while (c != 10 || inputInBlock) {
            consume();
            if (c == -1) {
                if (printTransform) {
                    return buf.toString() + ");";
                }
                return buf.toString();
            }
        }
        if (printTransform) {
            return buf.toString() + ");";
        }
        return buf.toString();
    }


    public static void consume() throws IOException {

        if (printTransform && c == (int)';' && !inDoubleQuotation && !inQuotation &&!inComment){
            c = (int)')';
        }else if (c == -1){
        }else{
            buf.append((char)c);
        }


        if (buf.toString().equals("print ")){
            buf = new StringBuilder();
            buf.append("System.out.println(");
            stack.push((int)'(');
            inputInBlock = true;
            printTransform = true;
        }

        if (c == (int)'/' && previous_c == (int)'/' && !inDoubleQuotation && !inQuotation){
            inComment = true;
        }

        if (!inComment){
            if (c == (int)'\''){
                inQuotation = !inQuotation;
            }else if (c == (int)'\"'){
                inDoubleQuotation = !inDoubleQuotation;
            }
        }


        if (!inQuotation && !inDoubleQuotation && !inComment){

            if (c == (int)'(' || c == (int)'[' || c == (int)'{'){

                stack.push(c);
                inputInBlock = true;

            }else if (c == (int)')' || c == (int)']' || c == (int)'}'){
                if (!stack.empty()){
                    stack.pop();
                    if (stack.empty()){
                        inputInBlock = false;
                    }
                }
            }

        }else if (inQuotation || inDoubleQuotation){
            inputInBlock = true;
        }

        previous_c = c;

        c = input.read();

        if (inComment){
            if (c == 10 || c == -1){
                inComment = false;
            }
        }
    }
}
